import os

from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Line
from kivy.uix.popup import Popup
from kivy.uix.textinput import TextInput
from kivymd.card import MDSeparator
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, ObjectProperty, BooleanProperty
from kivy.uix.modalview import ModalView
from kivymd.dialog import MDDialog
from widgets.button import OGButton

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'dialog.kv'))


class LoadingDialog(ModalView):
    pass


class OGMDDialog(MDDialog):

    dlg_type = StringProperty('normal')

    def __init__(self, **kwargs):
        super(OGMDDialog, self).__init__(**kwargs)

    def on_open(self, *args):
        super(OGMDDialog, self).on_open(*args)
        with self.canvas.after:
            Color(.25, .63, .8, 1) if self.dlg_type == 'normal' else Color(.7, 0, 0, 1)
            Line(width=1, rectangle=(self.x, self.y, self.width, self.height))


class ErrorDialog(OGMDDialog):
    dlg_type = 'error'
    message = StringProperty()


class YesNoDialog(OGMDDialog):

    message = StringProperty('')

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(YesNoDialog, self).__init__(**kwargs)

    def on_yes(self):
        self.dispatch('on_confirm')

    def on_confirm(self):
        pass


class InputDialog(OGMDDialog):

    text = StringProperty('')
    hint_text = StringProperty('')
    input_filter = ObjectProperty(None)

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(InputDialog, self).__init__(**kwargs)
        Clock.schedule_once(self.enable_focus, .3)
        Clock.schedule_once(self._update_text)

    def _update_text(self, *args):
        self.ids.input.text = self.text

    def on_yes(self):
        new_val = self.ids.input.text
        self.dismiss()
        self.dispatch('on_confirm', new_val)

    def on_confirm(self, *args):
        pass

    def enable_focus(self, *args):
        self.ids.input.focus = True


class CustomTextInput(TextInput):

    max_length = NumericProperty(50)

    def insert_text(self, substring, from_undo=False):
        super(CustomTextInput, self).insert_text(substring, from_undo)
        self.text = self.text[:50]


class TextInputDialog(OGMDDialog):

    text = StringProperty('')
    max_length = NumericProperty(50)

    def __init__(self, **kwargs):
        super(TextInputDialog, self).__init__(**kwargs)
        self.register_event_type('on_confirm')
        Clock.schedule_once(self.enable_focus, .3)

    def on_yes(self):
        new_val = self.ids.input.text
        self.dismiss()
        self.dispatch('on_confirm', new_val)

    def on_confirm(self, *args):
        pass

    def enable_focus(self, *args):
        self.ids.input.focus = True


class BraceletDetailDialog(OGMDDialog):
    bracelet_id = StringProperty()
    name = StringProperty('UNREGISTERED')
    perimeter = BooleanProperty(True)

    def __init__(self, **kwargs):
        super(BraceletDetailDialog, self).__init__(**kwargs)
        self.register_event_type('on_save')
        self.register_event_type('on_blink')
        self.register_event_type('on_delete')
        Clock.schedule_once(self.customize)

    def customize(self, *args):
        if self.name != 'UNREGISTERED':
            btn = OGButton(button_type='red', text='REMOVE')
            btn.bind(on_release=self.on_btn_remove)
            self.ids.box_buttons.add_widget(btn, 1)
            self.ids.box_buttons.size_hint_x = .9
        self.ids.name_button.text = 'Change Name' if self.name != 'UNREGISTERED' else 'Input Name'
        self.ids.switch_perimeter.active = self.perimeter
        grid = self.children[0].children[1]
        grid.padding = 16
        grid.remove_widget(grid.children[-1])
        grid.remove_widget(grid.children[-1])

    def on_btn_other(self):
        pass

    def on_btn_blink(self):
        self.dispatch('on_blink')

    def on_btn_save(self):
        if self.name == '':
            ErrorDialog(message='Please input name').open()
        else:
            val = {'name': self.name, 'perimeter': self.ids.switch_perimeter.active, 'bracelet_id': self.bracelet_id}
            self.dispatch('on_save', val)
            self.dismiss()

    def on_save(self, *args):
        pass

    def on_blink(self):
        pass

    def on_delete(self):
        pass

    def on_btn_name_button(self):
        val = '' if self.name == 'UNREGISTERED' else self.name
        dlg = InputDialog(text=val, hint_text=self.ids.name_button.text)
        dlg.bind(on_confirm=self.on_confirm)
        dlg.open()

    def on_confirm(self, *args):
        self.name = args[1]

    def on_btn_remove(self, *args):
        self.dismiss()
        self.dispatch('on_delete')


class OverDlg(OGMDDialog):
    dlg_type = 'error'

    def __init__(self, **kwargs):
        super(OverDlg, self).__init__(**kwargs)
        self.register_event_type('on_cancel')

    def on_btn_cancel(self):
        self.dispatch('on_cancel')
        self.dismiss()

    def on_cancel(self):
        pass
