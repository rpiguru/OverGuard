from kivy.factory import Factory
from kivy.properties import OptionProperty
from kivy.uix.button import Button
from kivy.clock import Clock
from kivy.uix.image import Image
from kivy.uix.label import Label


class OGButton(Button):

    font_name = 'EUROSTILE'
    font_size = 18
    bold = True

    button_type = OptionProperty("blue", options=['blue', 'gray', 'red'])

    def __init__(self, **kwargs):
        super(OGButton, self).__init__(**kwargs)
        self.background_down = 'assets/button_gray.jpg'
        self.bind(button_type=self._update_me)
        Clock.schedule_once(self._update_me)

    def _update_me(self, *args):
        self.background_normal = 'assets/button_{}.jpg'.format(self.button_type)


class OGImageButton(Image):

    def __init__(self, **kwargs):
        super(OGImageButton, self).__init__(**kwargs)
        self.register_event_type('on_release')

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos) and not self.disabled:
            self.dispatch('on_release')
        super(OGImageButton, self).on_touch_down(touch)

    def on_release(self):
        pass


class OGLabelButton(Label):

    font_name = 'EUROSTILE'

    def __init__(self, **kwargs):
        super(OGLabelButton, self).__init__(**kwargs)
        self.register_event_type('on_release')

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos) and not self.disabled:
            self.dispatch('on_release')
        super(OGLabelButton, self).on_touch_down(touch)

    def on_release(self):
        pass


class OGLabel(Label):
    font_name = 'EUROSTILE'


Factory.register('OGButton', cls=OGButton)
Factory.register('OGImageButton', cls=OGImageButton)
Factory.register('OGLabelButton', cls=OGLabelButton)
Factory.register('OGLabel', cls=OGLabel)
