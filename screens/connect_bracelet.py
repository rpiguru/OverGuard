import os
from functools import partial

import peewee
from kivy.config import _is_rpi
from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from db import Bracelet
from screens.base import BaseScreen
from widgets.button import OGButton
from widgets.dialog import BraceletDetailDialog

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'connect_bracelet.kv'))


class ConnectBraceletScreen(BaseScreen):

    bracelet_id = ''
    number = 5

    def on_enter(self, *args):
        super(ConnectBraceletScreen, self).on_enter(*args)
        if not _is_rpi:
            btn = OGButton(size_hint_x=None, width=120, text='TEST', button_type='gray')
            btn.bind(on_release=self.on_btn_test)
            self.ids.box_bottom.add_widget(btn, 1)

    def on_btn_back(self):
        self.switch_screen('setup_boat', 'right')

    def on_btn_skip(self, *args):
        self.switch_screen('menu', 'left')

    def on_btn_test(self, *args):
        self.on_new_bracelet('08:01:69:02:01:FC')

    @mainthread
    def on_new_bracelet(self, b_id, *args):
        self.bracelet_id = b_id
        dlg = BraceletDetailDialog(bracelet_id=b_id)
        dlg.bind(on_save=self.on_save_bracelet)
        dlg.bind(on_blink=self.on_blink)
        dlg.open()

    def on_save_bracelet(self, *args):
        b_info = args[1]
        try:
            item = Bracelet.get(Bracelet.bracelet_id == self.bracelet_id)
            item.name = b_info['name']
            item.perimeter = b_info['perimeter']
            item.save()
        except peewee.DoesNotExist:
            Bracelet.create(name=b_info['name'], bracelet_id=self.bracelet_id, perimeter=b_info['perimeter'])
        Clock.schedule_once(partial(self.send_bracelet_info, b_info))
        self.switch_screen('menu', 'left')

    def send_bracelet_info(self, b_info, *args):
        perimeter = 'ON' if b_info['perimeter'] else 'OFF'
        self.app.send_data(msg_type='RSP',
                           msg={'BRACELET_ID': self.bracelet_id, 'PERIMETER': perimeter, 'NAME': b_info['name']})
