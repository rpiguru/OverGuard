import os
from kivy.lang import Builder
from screens.base import BaseScreen

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'settings.kv'))


class SettingsScreen(BaseScreen):

    number = 8

    def on_btn_back(self):
        self.switch_screen('menu', 'right')
