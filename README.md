# OverGuard RPi Application

## Installation

    cd utils
    bash install.sh

And reboot!

- Here is the booting time of my RPi after following above:

        pi@raspberrypi:~ $ systemd-analyze
        Startup finished in 1.405s (kernel) + 4.739s (userspace) = 6.145s

- Reduce booting time

        sudo nano /boot/cmdline.txt

    + Replace `console=tty1` by `console=tty3` to redirect boot messages to the third console.
    + Add `loglevel=3` to disable non-critical kernel log messages.
    + Add `logo.nologo quiet` to the end of the line to remove the Raspberry PI logos from displaying

    Now, open `/etc/rc.local` and add another command:

        sudo nano /etc/rc.local

    Add `clear` just before the line exit 0.
    Add `dmesg --console-off` just before the line exit 0.

    Open `/boot/config.txt` and add below:

        enable_uart=1

    And disable HCI UART comm:

        sudo systemctl disable hciuart

    Disable rainbow splash screen:

        sudo nano /boot/config.txt
    Add this at the end of this file:

        disable_splash=1

    And reboot.