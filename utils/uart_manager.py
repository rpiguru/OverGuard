import json

import datetime
import serial


PREFIX = 'OG'


class UARTManager:

    ser = None

    def __init__(self, port='/dev/ttyS0', baudrate=115200, timeout=2):
        try:
            self.ser = serial.Serial(port=port, baudrate=baudrate, timeout=timeout)
        except Exception as e:
            print('Failed to initialize UART: {}'.format(e))

    def send_msg(self, msg_type='', msg=None):
        data = '+{}*{}*{}#\r\n'.format(PREFIX, msg_type, json.dumps(msg).replace(' ', ''))
        self._send_data(data)

    def _send_data(self, data):
        print('{} UART: Sending - {}'.format(datetime.datetime.now(), data))
        if self.ser:
            self.ser.write(data)

    def read_data(self):
        if self.ser:
            data = str(self.ser.readline())
            # Sample data: '+OG*CMD*{"UART":"TEST"}#\r\n'
            if data.startswith('+{}'.format(PREFIX)):
                try:
                    msg_type = data.split('*')[1]
                    message = json.loads(data.split('*')[2][:-3])
                    return {'type': msg_type, 'message': message}
                except Exception as e:
                    print('Failed to parse data(`{}`) - {}'.format(data, e))
