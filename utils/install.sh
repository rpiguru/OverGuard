#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install -y libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev \
    pkg-config libgl1-mesa-dev libgles2-mesa-dev \
    python-setuptools libgstreamer1.0-dev git-core \
    gstreamer1.0-plugins-{bad,base,good,ugly} \
    gstreamer1.0-{omx,alsa} python-dev libmtdev-dev \
    xclip

sudo pip2 install Cython==0.25.2

sudo pip2 install git+https://github.com/kivy/kivy.git@master

sudo apt-get install -y python-serial sqlite3


cp ../assets/boot.mp4 ~
sudo cp splashscreen.service /etc/systemd/system/splashscreen.service
sudo systemctl enable splashscreen
