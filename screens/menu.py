import os
from kivy.lang import Builder
from screens.base import BaseScreen

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'menu.kv'))


class MenuScreen(BaseScreen):

    number = 6

    def on_btn_list(self):
        self.switch_screen('list', 'down')

    def on_btn_settings(self):
        self.switch_screen('settings', 'left')

    def on_btn_back(self):
        self.switch_screen('connect_bracelet', 'right')

