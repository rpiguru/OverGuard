import os
from peewee import *

db = SqliteDatabase(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'db.sqlite'))


class Bracelet(Model):

    name = TextField()
    bracelet_id = TextField()
    perimeter = BooleanField()

    class Meta:
        database = db
